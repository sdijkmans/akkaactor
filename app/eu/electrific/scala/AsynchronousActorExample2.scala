package eu.electrific.scala

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.concurrent.Await
import akka.actor._
import akka.pattern.ask
import akka.util.Timeout

case class StringHolder(word:String)

class StringReturner extends Actor {
  
  override def receive = {
    case StringHolder(args:String) => sender ! args
  }
    
}


object Main extends App {
  implicit val timeout: Timeout = 100 milliseconds
  val system = ActorSystem("HelloSystem")
  val giveMeBackSomething = system.actorOf(Props[StringReturner], name = "givemebacksomething")
  val future = giveMeBackSomething ? StringHolder("Hey, can you send this message back to me?")
  val result = Await.result(future, timeout.duration).asInstanceOf[String]
  println(result)
}