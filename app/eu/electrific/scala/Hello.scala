package eu.electrific.scala

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import akka.actor.Actor
import akka.actor.ActorSystem
import akka.pattern.ask
import akka.util.Timeout
import akka.actor.Props


object PubSubMediator {
  final val Name = "pub-sub-mediator"
}

class HelloActor extends Actor {
  override def receive = {
    case "hello" => println("hello back at you")
    case "anyone home?" => println("nope")
    case _       => println("huh?")
  }
}


object Main2 extends App {
  val system = ActorSystem("HelloSystem")
  // default Actor constructor
  val helloActor = system.actorOf(Props[HelloActor], name = "helloactor")
  helloActor ! "hello"
  helloActor ! "anyone home?"
  helloActor ! "buenos dias"
  
}